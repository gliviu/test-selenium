//import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.time.Duration;


public class SeleniumTest {
    @Test
    void t1(){
        System.setProperty("webdriver.chrome.driver", "/home/liviu/bin/chrome-for-testing/chromedriver-linux64/chromedriver");
        ChromeOptions options = new ChromeOptions();
        options.setBinary("/home/liviu/bin/chrome-for-testing/chrome-linux64/chrome");
        WebDriver driver = new ChromeDriver(options);
        driver.manage().timeouts().implicitlyWait(Duration.ofMillis(500));  // todo improve

        driver.get("https://duckduckgo.com/");

        WebElement searchBox = driver.findElement(By.name("q"));
        searchBox.sendKeys("aaa");
        searchBox.sendKeys(Keys.ENTER);

        WebElement firstResult = driver.findElement(By.id("r1-0"));
        System.out.println(firstResult.getText());

        driver.quit();
    }
}
