
* Download chrome and chromedriver from https://googlechromelabs.github.io/chrome-for-testing/
  * Place chrome under `/home/liviu/bin/chrome-for-testing/chrome-linux64/chrome`.
  * Place driver under `/home/liviu/bin/chrome-for-testing/chromedriver-linux64/chromedriver`.
* Then 'mvk package'. This will run the selenium test.